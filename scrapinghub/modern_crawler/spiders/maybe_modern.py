# -*- coding: utf-8 -*-
import scrapy
from modern_crawler.items import PageItem
from modern_crawler.utils import extract_dimension


class MaybeModernSpider(scrapy.Spider):
    name = 'maybemodern'
    allowed_domains = ['pstrial-2018-05-21.toscrape.com']
    start_urls = ['http://pstrial-2018-05-21.toscrape.com/browse/']

    def parse(self, response):
        '''Parse the root level from browser category, this parse
        method is responsible to select our two initial categories'''

        for link in response.css('a'):
            next_page = response.urljoin(
                        link.css('::attr(href)').extract_first())
            category = link.css('h3::text').extract_first()

            if category and category in ['In Sunsh', 'Summertime']:
                item = PageItem()
                item['categories'] = [category]
                request = scrapy.Request(next_page,
                                         callback=self.parse_category)
                request.meta['item'] = item
                yield request

    def parse_category(self, response):
        '''Parse category page, extract categories links,
        store that category name and pass away to next request'''
        item = response.meta.get('item')
        category_links = response.css('#subcats div a')

        for category_link in category_links:
            category_name = category_link.css('h3::text').extract_first()
            category_link = category_link.css('::attr(href)').extract_first()

            if category_name:
                # Append category to item
                sub_item = PageItem()
                upper_categories = item.get('categories')
                sub_item['categories'] = upper_categories + [category_name]

                # Create the next request
                next_page = response.urljoin(category_link)
                next_request = scrapy.Request(next_page, self.parse_category)
                next_request.meta['item'] = sub_item

                yield next_request

        if not category_links:
            art_links = response.xpath(
                '//*[@id="body"]/div[2]/a/@href').extract()

            filtered_links = [link for link in art_links if not 'page' in link]

            if not filtered_links:
                return

            for next_link in art_links:
                # Create item page request
                next_page = response.urljoin(next_link)

                if 'page' in next_link:
                    next_request = scrapy.Request(next_page,
                                                  self.parse_category)
                    next_request.meta['item'] = item
                else:
                    next_request = scrapy.Request(next_page, self.parse_item)
                    next_request.meta['item'] = item

                yield next_request

    def parse_item(self, response):
        '''Parse art's page as the last page to craw after reach category
        end.'''
        categories = response.meta.get('item').get('categories')
        raw_dimensions = response.xpath(
            '//*[@id="content"]/dl/dd[3]/text()').extract_first()
        dimensions = extract_dimension(raw_dimensions)
        image = response.css('img::attr(src)').extract_first()
        description = response.css(
            '[itemprop="description"] p::text').extract_first()
        artist = response.css('[itemprop="artist"]::text').extract_first()

        yield {
            'url': response.url,
            'artist': artist,
            'title': response.css('[itemprop="name"]::text').extract_first(),
            'image': response.urljoin(image),
            'height': dimensions[0],
            'width': dimensions[1],
            'description': description,
            'path': categories,
        }
