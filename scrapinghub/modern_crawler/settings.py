BOT_NAME = 'modern_crawler'

SPIDER_MODULES = ['modern_crawler.spiders']
NEWSPIDER_MODULE = 'modern_crawler.spiders'

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'modern_crawler.pipelines.ModernCrawlerPipeline': 300,
}