# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ModernCrawlerPipeline(object):
    def process_item(self, item, spider):
        clone_item = {}

        for key, value in item.items():
            if value is not None:
                clone_item[key] = value

        return clone_item
