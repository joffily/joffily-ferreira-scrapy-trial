'''Util module containg functions that are useful to do the work on
spider parsing'''
import re


def extract_dimension(raw_dimesions):
    '''extract dimensions with required logic
    if the dimension in cm are presents we should return that
    otherwise we should return the inch dimesions in float.

    Note: This regex is not completely working for every cases
    '''

    if raw_dimesions is None:
        return [None, None]

    if 'cm' not in raw_dimesions:
        return [None, None]

    regex = '([\w\s\/-]+)\s*in(\.\s\((.*)cm\){0,1})*'
    regex_match = re.match(regex, raw_dimesions)

    if regex_match is None:
        return [None, None]

    dimensions = regex_match.groups()

    if dimensions[2] is None:
        return [None, None]

    dimensions = dimensions[2].strip().split(' x ')

    if len(dimensions) == 0:
        return [None, None]
    elif len(dimensions) == 1:
        return dimensions + [None]
    else:
        return dimensions
